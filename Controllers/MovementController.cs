using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InventarioApi.Data;
using InventarioApi.Models;

namespace InventarioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovementController : ControllerBase
    {
        private readonly AppDbContext _context;

        public MovementController(AppDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> PostMovement(Movement_product movement)
        {
            if (movement.Movement_type != "Entrada" && movement.Movement_type != "Salida")
            {
                return BadRequest("El tipo de movimiento debe ser 'Entrada' o 'Salida'.");
            }
            if (movement.Amount <= 0)
            {
                return BadRequest("La cantidad debe ser mayor a 0.");
            }

            var product = await _context.Product.FindAsync(movement.Product_id);
            if (product == null)
            {
                return NotFound();
            }

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (movement.Movement_type == "Entrada")
                    {
                        product.Amount += movement.Amount;
                    }
                    else if (movement.Movement_type == "Salida")
                    {
                        product.Amount -= movement.Amount;
                    }

                    _context.Movement_product.Add(movement);
                    await _context.SaveChangesAsync();
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return Ok(product);
        }
    }
}
