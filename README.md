# InventarioApi

_InventarioApi es una prueba de API REST desarrollada en ASP.NET Core, que utiliza PostgreSQL como base de datos. La API permite gestionar un inventario básico._

## Requisitos
* PostgreSQL

## Configuración
1. Clona el repositorio:
```
git clone https://gitlab.com/cs5034924/inventario.git
cd InventarioApi
```

2. Configura la cadena de conexión en appsettings.json:
```
{
  "ConnectionStrings": {
    "DefaultConnection": "Host=localhost;Database=YourDatabaseName;Username=YourUsername;Password=YourPassword"
  },
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "AllowedHosts": "*"
}
```

3. Restaura los paquetes:
```
dotnet restore
```

4. Ejecuta la aplicación:
```
dotnet run
```

## Endpoints
* http://localhost:5035/api/product
* http://localhost:5035/api/movement
  
## Notas
Asegúrate de que PostgreSQL esté en ejecución y que la cadena de conexión en appsettings.json esté correctamente configurada.
La base de datos se debe inicializar con el archivo script.sql.
