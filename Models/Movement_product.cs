namespace InventarioApi.Models
{
    public class Movement_product
    {
        public int Id { get; set; }
        public required int Product_id { get; set; }
        public required string Movement_type { get; set; }
        public required int Amount { get; set; }
        public DateTime Created_date { get; set; }
    }
}
