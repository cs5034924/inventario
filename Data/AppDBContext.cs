using Microsoft.EntityFrameworkCore;
using InventarioApi.Models;

namespace InventarioApi.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Product> Product { get; set; }
        public DbSet<Movement_product> Movement_product { get; set; }

        // protected override void OnModelCreating(ModelBuilder modelBuilder)
        // {
            // modelBuilder.Entity<Product>().ToTable("Product");
            // modelBuilder.Entity<Movement_product>().ToTable("Movement_product");
        // }
    }
}
