DROP DATABASE IF EXISTS inventario;

CREATE DATABASE inventario
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Colombia.1252'
    LC_CTYPE = 'Spanish_Colombia.1252'
    LOCALE_PROVIDER = 'libc'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;

CREATE TABLE IF NOT EXISTS public."Product"
(
    "Id" integer NOT NULL DEFAULT nextval('"Product_Id_seq"'::regclass),
    "Name" character varying COLLATE pg_catalog."default" NOT NULL,
    "Amount" integer NOT NULL DEFAULT 0,
    CONSTRAINT "Product_pkey" PRIMARY KEY ("Id")
)

CREATE TABLE IF NOT EXISTS public."Movement_product"
(
    "Id" integer NOT NULL DEFAULT nextval('"Movement_product_Id_seq"'::regclass),
    "Product_id" integer NOT NULL,
    "Movement_type" character varying COLLATE pg_catalog."default" NOT NULL,
    "Amount" integer NOT NULL,
    "Created_date" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "Movement_product_pkey" PRIMARY KEY ("Id"),
    CONSTRAINT "Product_id" FOREIGN KEY ("Product_id")
        REFERENCES public."Product" ("Id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

CREATE TABLE IF NOT EXISTS public."User"
(
    "Id" integer NOT NULL DEFAULT nextval('"User_Id_seq"'::regclass),
    "Name" character varying COLLATE pg_catalog."default" NOT NULL,
    "Email" character varying COLLATE pg_catalog."default" NOT NULL,
    "Password" character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "User_pkey" PRIMARY KEY ("Id")
)